#!/bin/bash

docker_pull_and_run() {
	local docker_image=$1
	local test_file=$2

	echo
	echo "########################################################################"

	echo "INFO: Testing '${docker_image}'"

	docker pull ${docker_image} > /dev/null 2>&1
	docker run --rm -ti -e DEBIAN_FRONTEND=noninteractive -e TZ=Etc/UTC -e CLEAN_ENV=1 -v $(pwd)/../packages:/packages -v $(pwd)/${test_file}:/opt/asdf/test.sh -w /opt/asdf ${docker_image} bash test.sh 2>&1 | tee -a test_asdf_plugins.log
	# docker run --rm -ti -e DEBIAN_FRONTEND=noninteractive -e TZ=Etc/UTC -e CLEAN_ENV=1 -v $(pwd)/../packages:/packages -v $(pwd)/${test_file}:/opt/asdf/test.sh -w /opt/asdf ${docker_image} bash

	echo "########################################################################"
}

rm -f test_asdf_plugins.log

# Ubuntu
#
docker_pull_and_run ubuntu:21.10 test_asdf_plugins_ubuntu.sh
docker_pull_and_run ubuntu:20.04 test_asdf_plugins_ubuntu.sh
docker_pull_and_run ubuntu:18.04 test_asdf_plugins_ubuntu.sh

# Debian
#
docker_pull_and_run debian:11 test_asdf_plugins_ubuntu.sh
docker_pull_and_run debian:10 test_asdf_plugins_ubuntu.sh
docker_pull_and_run debian:9 test_asdf_plugins_ubuntu.sh

# Arch
#
# FIXME: TODO
# docker_pull_and_run archlinux:base test_asdf_plugins_arch.sh
