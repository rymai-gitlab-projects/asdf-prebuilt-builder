#!/bin/bash

set -euo pipefail

apt-get -qq update && apt-get -y install sudo git make curl vim libatomic1 libreadline-dev lsb-release > /dev/null 2>&1

mkdir -p /opt/asdf || true
cd /opt/asdf && [[ ! -d .asdf ]] && git clone https://github.com/asdf-vm/asdf.git .asdf --branch v0.9.0 > /dev/null 2>&1

export ASDF_DIR=/opt/asdf
export ASDF_DATA_DIR=/opt/asdf
source .asdf/asdf.sh

asdf plugin add postgres https://gitlab.com/ashmckenzie/asdf-prebuilt-plugin.git || true > /dev/null 2>&1
asdf plugin add redis https://gitlab.com/ashmckenzie/asdf-prebuilt-plugin.git || true > /dev/null 2>&1
asdf plugin add ruby https://gitlab.com/ashmckenzie/asdf-prebuilt-plugin.git || true > /dev/null 2>&1

cat << EOS > .tool-versions
postgres 12.9
redis 6.0.16
ruby 2.7.5 3.0.2
EOS

asdf install

echo
echo "$(lsb_release -s -i)_$(lsb_release -s -r)" | tr A-Z a-z
echo

check_for() {
  local cmd="${1}"

  asdf which "${cmd}"
  eval "${cmd} --version"
  echo
}

check_for "ruby"
check_for "psql"
check_for "redis-server"
